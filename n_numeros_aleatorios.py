#!/usr/bin/env python3
# -*- coding: utf8 -*-
"""
Generar números aleatorios entre 1 y 1000 y obtener 5 distintos
que sean divisibles por 13.
"""

# RUBENS 
import random
divisible = 13
lista_divisibles = []
#Inicio - while
while len(lista_divisibles) < 5:
    #Generamos un aleatorio
    num_aleatorio = random.randint(1, 1000)
    
    #Inicio de bloque - determinamos si es divisible por (divisible)
    if num_aleatorio % divisible == 0:
        #si es divisible
        if len(lista_divisibles) > 0:
            #if lista_divisibles is num_aleatorio:
            if num_aleatorio in lista_divisibles:
                pass
            else:
                lista_divisibles.append(num_aleatorio)    
        else:
            lista_divisibles.append(num_aleatorio) 
    # Fin de bloque  - determinamos si es divisible por (divisible)
#Fin - while 
                  
#resultado final
print(lista_divisibles)

